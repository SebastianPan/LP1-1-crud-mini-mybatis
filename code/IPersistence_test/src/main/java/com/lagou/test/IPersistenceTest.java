package com.lagou.test;

import com.lagou.dao.IUserDao;
import com.lagou.io.Resources;
import com.lagou.pojo.User;
import com.lagou.sqlSession.SqlSession;
import com.lagou.sqlSession.SqlSessionFactory;
import com.lagou.sqlSession.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

public class IPersistenceTest {

    @Test
    public void test() throws Exception {
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        SqlSession sqlSession = sqlSessionFactory.openSession();

        long millis = System.currentTimeMillis();
        IUserDao userDao = sqlSession.getMapper(IUserDao.class);
        //增加
        User user = new User();
        user.setUsername("王松"+millis);
        userDao.insert(user);
        user.setUsername("李二"+System.currentTimeMillis());
        userDao.insert(user);
        user.setUsername("赵武"+System.currentTimeMillis());
        userDao.insert(user);
        System.out.println("===============添加完成==============");

        //查询
        List<User> all = userDao.findAll();
        System.out.println("===============查询所有完成==============");
        for (User user1 : all) {
            if(user1.getUsername().equals("王松"+millis)){
                //删除
                userDao.delete(user1.getId());
                System.out.println("===============删除完成==============");
            }
        }

        User user1 = userDao.findByCondition(user);
        System.out.println("===============条件查询完成==============");
        System.out.println(user1);
        //更新
        user1.setUsername("赵武更"+System.currentTimeMillis());
        userDao.update(user1);
        System.out.println("===============更新完成==============");

    }



}
